﻿name_list_kobold = {

	dynasty_names = {
		#WIP
		Scalesinger Scalemaster Greatfang Sneakscale Dragontail Sharpjaw Trapsetter Trapkin Spikemaker Scaleburrow Spiketail Traptail
		Lowear Hardwax Broadnose Smallbristle Talltooth Goodflame Losteye Smallpick Smallspike Smalltail Lazyteeth Trapsmith Trapmaster
		Guttersneak Burrowsneaker Trapsneak Grimeguard Bonebrand Bronzedelver Minedelver Minespike Minecrest Emberclaw
		Nimbleclaw Trapdagger Rumbledagger Minedagger Quickdagger Darktrap Fangsplitter Fangclaw Trapfang Slipscale
		
	}

	dynasty_names = {
		#WIP
		Scalesinger Scalemaster Greatfang Sneakscale Dragontail Sharpjaw Trapsetter Trapkin Spikemaker Scaleburrow Spiketail Traptail
		Lowear Hardwax Broadnose Smallbristle Talltooth Goodflame Losteye Smallpick Smallspike Smalltail Lazyteeth Trapsmith Trapmaster
		Guttersneak Burrowsneaker Trapsneak Grimeguard Bonebrand Bronzedelver Minedelver Minespike Minecrest Emberclaw
		Nimbleclaw Trapdagger Rumbledagger Minedagger Quickdagger Darktrap Fangsplitter Fangclaw Trapfang Slipscale
		
	}
	
	
	male_names = {
		Arru Deekin Vrik Nark Drighad Grator Zurzu Zurzumex Nalnaz Vud Dak Nak Irto Ogra Kovu Egi Miglo Vitru Ekbo Marku Zold Savlet Mavlik Sozrir Znorbriz Ravrurt Erkil Grigze Qupzax 
		Dhan Mork Dizzap Dhesesk Realp Dhild Vhitir Itrol Zipzek Qradez Qalk Aakax Dheep Rax Nerlisk Zarril Zux Adrax Jukop Vren Qirnux
	}
	female_names = {
		Eru Tegil Ikse Uho Gakar Laadaa Lerlam Sersa Alra Vakkra
	}
	#TODO
	#dynasty_of_location_prefix = "" 

	pat_grf_name_chance = 50
	mat_grf_name_chance = 5
	father_name_chance = 10
	
	pat_grm_name_chance = 10
	mat_grm_name_chance = 50
	mother_name_chance = 5

	mercenary_names = {
		#TODO
	}
}