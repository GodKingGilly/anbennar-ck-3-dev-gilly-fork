﻿magic_mastery = {
	parent = skills
	alias = { magical_mastery magic_mastery_i }

	texture = "gfx/interface/icons/icon_magic_mastery.dds"
}

court_mage = {
	parent = councillor
	alias = { court_mages }
}

racial_legitimacy = {
	alias = { legitimate_race illegitimate_race }
}

calindal = {
}

bladestewards = {
	alias = { bladesteward order_of_bladestewards }
}

quest = {
	alias = { quests }
}

mages = {
	texture = "gfx/interface/icons/regimenttypes/skirmishers.dds"
	parent = men_at_arms
}

beasts = {
	texture = "gfx/interface/icons/regimenttypes/camel_riders.dds"
	parent = men_at_arms
}