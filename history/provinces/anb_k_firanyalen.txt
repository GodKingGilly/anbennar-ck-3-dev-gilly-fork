#k_firanyalen
##d_firanyalen
###c_firanyalen
676 = {		#Firanyalen

    # Misc
    culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}

3001 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

3002 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = church_holding

    # History
}

3003 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

###c_ijenmyfor
677 = {		#Ijenmyfor

    # Misc
    culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}

3004 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

3005 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

###c_kotierontol
678 = {		#Kotierontol

    # Misc
    culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}

3006 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

3007 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

###c_qarvetarqan
675 = {		#Qarvetarqan

    # Misc
    culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}

3008 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

3009 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_eagle
	holding = none

    # History
}

##d_misarallen
###c_mionfo
672 = {		#Mionfo

    # Misc
    culture = firanyalyra
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}

3013 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

3014 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

###c_misarallen
673 = {		#Misarallen

    # Misc
    culture = firanyalyra
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}

3010 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

3011 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

3012 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

###c_mynaurame
674 = {		#Mynaurame

    # Misc
    culture = firanyalyra
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}

3015 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

3016 = {

    # Misc
	 culture = firanyalyra
    religion = hunt_of_the_myna
	holding = none

    # History
}

##d_nansalen
###c_nansalen
670 = {		#Nansalen

    # Misc
	culture = nansalyra
    religion = hunt_of_the_crane
	holding = tribal_holding

    # History
}

3017 = {

    # Misc
	 culture = nansalyra
    religion = hunt_of_the_crane
	holding = none

    # History
}

3018 = {

    # Misc
	 culture = nansalyra
    religion = hunt_of_the_crane
	holding = church_holding

    # History
}

###c_vainviria

6349 = {		#Vainviria

    # Misc
    culture = nansalyra
    religion = hunt_of_the_crane
	holding = tribal_holding

    # History
}

6347 = {		#Tenurame

    # Misc
    culture = nansalyra
    religion = hunt_of_the_crane
	holding = none

    # History
}

6348 = {		#Syphetauhun

    # Misc
    culture = nansalyra
    religion = hunt_of_the_crane
	holding = none

    # History
}

###c_khaajesfo
4117 = {

    # Misc
    culture = nansalyra
    religion = hunt_of_the_crane
	holding = tribal_holding

    # History

}
3019 = {

    # Misc
	 culture = nansalyra
    religion = hunt_of_the_crane
	holding = none

    # History
}