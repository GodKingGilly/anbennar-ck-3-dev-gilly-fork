383 = { #Desh-al-Deshak

    # Misc
    culture = khasani
    religion = father_aiji
	holding = castle_holding

    # History
}

384 = { #Adseenu

    # Misc
    culture = khasani
    religion = father_amahash
	holding = castle_holding

    # History
}

385 = { #Addsen

    # Misc
    culture = khasani
    religion = mother_akhat
	holding = castle_holding

    # History
}

386 = { #Akasat

    # Misc
    culture = khasani
    religion = mother_akhat

    # History
}

393 = { #Kokarrat

    # Misc
    culture = stonewing
    religion = father_aiji

    # History
}

395 = { #Srihaf

    # Misc
    culture = stonewing
    religion = mother_akasik
	holding = castle_holding

    # History
}

396 = { #Thik

    # Misc
    culture = stonewing
    religion = mother_akhat
	holding = castle_holding

    # History
}

398 = { #Akora

    # Misc
    culture = khasani
    religion = mother_akhat
	holding = castle_holding

    # History
}

400 = { #Khasa

    # Misc
    culture = khasani
    religion = mother_akhat
	holding = castle_holding

    # History
}

401 = { #Akbia

    # Misc
    culture = khasani
    religion = father_aiji
	holding = castle_holding

    # History
}