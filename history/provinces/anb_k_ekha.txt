387 = { #Sensha

    # Misc
    culture = ekhasi
    religion = mother_akhat

    # History
}

388 = { #

    # Misc
    culture = ekhasi
    religion = father_amahash

    # History
}

389 = { #Benirkes

    # Misc
    culture = ekhasi
    religion = father_amahash

    # History
}

390 = { #Sirik

    # Misc
    culture = ekhasi
    religion = mother_akasik

    # History
}

391 = {	#Habnits

    # Misc
    culture = ekhasi
    religion = father_aiji

    # History
}

392 = { #Sirspit

    # Misc
    culture = ekhasi
    religion = mother_akasik

    # History
}

402 = { #Ekha

    # Misc
    culture = ekhasi
    religion = father_amahash

    # History
}

406 = { #Uwak

    # Misc
    culture = ekhasi
    religion = father_amahash
	holding = castle_holding

    # History
}

407 = { #Bhetu

    # Misc
    culture = ekhasi
    religion = father_amahash

    # History
}

2937 = { #Tipspit

    # Misc
    culture = ekhasi
    religion = father_amahash

    # History

}
