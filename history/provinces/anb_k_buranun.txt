#k_buranun
##d_kumarses
###c_kumarses
591 = {		#Kumarses

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = castle_holding

    # History
}

6541 = {		#Apaskumar

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = city_holding

    # History
}

6542 = {		#Sebskeru

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none

    # History
}

6543 = {		#Fulayus

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = church_holding

    # History
}

6544 = {		#Karqaslu

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = city_holding

    # History
}

###c_gerxhoraak
610 = {		#Gerxhoraak

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = castle_holding

    # History
}

6547 = {		#Waksurr

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = city_holding

    # History
}

6548 = {		#Agtpittar

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none

    # History
}

6549 = {		#Sarkaliyya

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = church_holding

    # History
}

###c_sap_idiqlat
603 = {		#Idiqlatbar

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = city_holding

    # History
}

6545 = {		#Sap Idiqlat

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none

    # History
}

6546 = {		#Eduz-Zummurkt

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = church_holding

    # History
}

##d_gisshuram
###c_varamhar
614 = {		#Varamhar

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = castle_holding

    # History
}

6553 = {		#Kahilsu

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

6554 = {		#Traz Idiqlat

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none

    # History
}

6555 = {		#Akoskumar

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}

###c_gisshuram
608 = {		#Gisshuram

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = city_holding

    # History
}

6556 = {		#Sagklum

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = church_holding

    # History
}

6557 = {		#Dalarand

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = castle_holding

    # History
}

6558 = {		#Surubittu

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = city_holding

    # History
}

6559 = {		#Traz Buranun

    # Misc
    culture = zanite
    religion = rite_of_the_dancing_fire
	holding = none

    # History
}

###c_ulmis_idiqlat
609 = {		#Zandazar

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = castle_holding

    # History
}

6550 = {		#Nissardu

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = none

    # History
}

6551 = {		#Tesuhar

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = church_holding

    # History
}

6552 = {		#Mahabar

    # Misc
    culture = sun_elvish
    religion = sundancer_paragons
	holding = city_holding

    # History
}


##d_anzarzax
###c_anzarzax
590 = {		#Anzarzax

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = castle_holding

    # History
}

6539 = {		#Sukarzan

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = none

    # History
}

6540 = {		#Vremasan

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = city_holding

    # History
}

###c_linkum
589 = {		#Linkum

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = city_holding

    # History
}

6537 = {		#Azkahres

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = castle_holding

    # History
}

6538 = {		#Zhugkalis

    # Misc
    culture = barsibu
    religion = cult_of_mourning
	holding = none

    # History
}