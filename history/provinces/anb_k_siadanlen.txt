#k_siadanlen
##d_siadanlen
###c_siadunaiun
2904 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}
6747 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = none

    # History
}
6748 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = church_holding

    # History
}
###c_nuzidastu
2903 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}
6749 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = none

    # History
}
###c_garanias
2920 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}
6750 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = none

    # History
}
##d_baganas
###c_baganas
2901 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = tribal_holding

    # History
}
6751 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = none

    # History
}
###c_ardumu
2900 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = tribal_holding

    # History
}
6752 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = none

    # History
}
###c_daqanza
2902 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = tribal_holding

    # History
}
6753 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = none

    # History
}
##d_harnio
###c_harnio
2916 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = tribal_holding

    # History
}
6754 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = none

    # History
}
###c_ianvireri
2917 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}
6755 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = none

    # History
}
##d_mermigan
###c_mermigan
2921 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}
6756 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = none

    # History
}
6757 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = none

    # History
}
###c_kotidurnor
2919 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}
6758 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = none

    # History
}
###c_tarnuthen
2918 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}
6759 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = none

    # History
}
###c_chorfo
2922 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}
6760 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = none

    # History
}
##d_siphefireri
###c_siphefireri
2906 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}
6761 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = none

    # History
}
###c_larfira
2908 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = tribal_holding

    # History
}
6762 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = none

    # History
}
6763 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_myna
	holding = none

    # History
}
###c_istartovo
4376 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = tribal_holding

    # History
}
6764 = {

    # Misc
    culture = siadannira
    religion = hunt_of_the_eagle
	holding = none

    # History
}
###c_nasratrub
2910 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = tribal_holding

    # History
}
6765 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = none

    # History
}
##d_ebbusubtu
###c_ebbusubtu
2909 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = tribal_holding

    # History
}
6766 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = none

    # History
}
6767 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = none

    # History
}
###c_shibahurran
2907 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = tribal_holding

    # History
}
6768 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = none

    # History
}
###c_trasenarbu
2905 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = tribal_holding

    # History
}
6769 = {

    # Misc
    culture = masnsih
    religion = cult_of_the_sands
	holding = none

    # History
}