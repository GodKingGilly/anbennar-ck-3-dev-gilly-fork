k_buranun = {
	1006.6.22 = {
		liege = e_bulwar
		holder = sun_elvish0022	#Gelmonias
	}
}

d_gisshuram = {
	1006.6.22 = {
		liege = k_buranun
		holder = sun_elvish0022	#Gelmonias
	}
}

c_varamhar = {
	1000.1.1 = { change_development_level = 16 }
	1006.6.22 = {
		liege = d_gisshuram
		holder = sun_elvish0025	# Varamel Varamzuir
	}
}

c_linkum = {
	1000.1.1 = { change_development_level = 6 }
}

c_anzarzax = {
	1000.1.1 = { change_development_level = 5 }
}

c_kumarses = {
	1000.1.1 = { change_development_level = 12 }
}

c_gerxhoraak = {
	1000.1.1 = { change_development_level = 6 }
}

c_sap_idiqlat = {
	1000.1.1 = { change_development_level = 9 }
}

c_ulmis_idiqlat = {
	1000.1.1 = { change_development_level = 12 }
}

c_gisshuram = {
	1000.1.1 = { change_development_level = 19 }
	1006.6.22 = {
		liege = e_bulwar
		holder = sun_elvish0022	#Gelmonias
	}
}